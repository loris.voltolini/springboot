package com.example.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name="attori")
//Lombok 
@Getter @Setter @ToString //Genera automaticamente tutti i gettere, i setter e i ToString necessari
public class Attore {
    @Id //Attribuisce la chiave primaria all'attributo sotto di se
    @GeneratedValue(strategy =  GenerationType.IDENTITY) //auto_increment
    @Column(name="cod_attore") //Specifica il nome che avrà il campo in sql
    Long codAttore;
    String nome;
    @Column(name="cod_nascita")
    Long annoNascita;
    String country;
}

