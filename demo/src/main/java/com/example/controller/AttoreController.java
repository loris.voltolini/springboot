package com.example.controller;

import com.example.domain.Attore;
import com.example.service.AttoreService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.*;

@RestController
@RequestMapping("/api/attori")//percorso di base per i root e gli entrry point
@CrossOrigin(origins = "*", maxAge = 3600 )//probelma del corssOrigin; controlla da chi arrivano le richieste sul browser
public class AttoreController {

    //Istanziamento dell'oggetto dipendente dal framewor
    @Autowired
    AttoreService attoreService;

    @GetMapping()
    public ResponseEntity<?> findAll(){
        return new ResponseEntity<List<Attore>>(attoreService.FindAll(),HttpStatus.OK);
    }
//api PER RITORNARE UN sINGOLO ELEMENTO
    @GetMapping("/{codAttore}")
  
        public ResponseEntity<?> findById(@PathVariable("codAttore") Long codAttore)
            {
                //return new ResponseEntity<Attore>(attoreService.findById(codAttore),HttpStatus.OK);
                Optional<Attore> optAttore= attoreService.findById(codAttore);
                if(optAttore.isPresent())// controlla se l'alttore è stato trovato
                    {
                        Attore attore = optAttore.get();// recupero l'ogetto di tipo attore
                        return new ResponseEntity<Attore>(attore,HttpStatus.OK);
                    }
                return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
            }
//POST PER AGGIUNGER UN ELEMENTO    
    @PostMapping()
    public ResponseEntity<?> save(@RequestBody Attore attore)
        {
            return new ResponseEntity<Void>(HttpStatus.OK);
        }

}


