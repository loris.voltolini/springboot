package com.example.service;

import java.util.List;
import java.util.Optional;

import com.example.domain.Attore;
import com.example.repository.AttoreRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service //logica di buisness, avrà tanti metodi quanti sono i root che ci servono per API
public class AttoreService {
    @Autowired //Istanzia gli ogetti sottostanti
    AttoreRepository attoreRepository; // del repository

    //metodo che restituisce tutti gli attori contenuti nella tabella Attori
    public List<Attore> FindAll()
        { 
                return attoreRepository.findAll();// metodi già creati dal freimwork
        }
// restiuisce un ogetto di tipo Optional>Attore>: 1)isPresent()=true trovato 2)false= non trovato
    public Optional<Attore> findById(Long codAttore)
        {
            return attoreRepository.findById(codAttore);
        }
//Salva l'attore all'interno del database
    public void save(Attore attore)
        {
            attoreRepository.save(attore);
        }
//metodo per eliminare
    public Optional<Attore> deleteById(Long codAttore)
        {
            Optional<Attore> optAttore=attoreRepository.findById(codAttore);
            if(optAttore.isPresent())
                {
                    attoreRepository.deleteById(codAttore);
                //restituisce se la trovato True/false e chi è stato cancellato
                    return optAttore;
                }
            return Optional.empty();
           
        }
}
