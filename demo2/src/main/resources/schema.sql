
create table if not exists countries(
    country varchar(20) Not NULL,
    primary key(country)
);

create table if not exists note(
    Id BIGINT(20) NOT NULL,
    note LONGTEXT NULL DEFAULT NULL,
    primary key (Id)
);

create table if not exists attori(
    cod_attore bigint auto_increment primary key,
    cod_nascita bigint,
    country varchar(255),
    nome varchar(255),
    nota_id BIGINT(20),
    -- index idx_nota_id_attori(nota_id ASC),
    constraint fk_nota_id_note foreign key (nota_id) references note(Id),
    constraint fk_countries_country foreign key (country) references countries(country)
);