package com.example.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name="countries")
@Getter @Setter @ToString 
public class Country {
    @Id //Attribuisce la chiave primaria all'attributo sotto di se
    @Column(name="country",
            length = 20,
            nullable = false) 
    String country;

    @OneToMany(
        mappedBy ="country",
        cascade = CascadeType.ALL,
        orphanRemoval = true 
    )

    @JsonIgnore
    Set<Attore> attore;

}

