package com.example.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name="attori")
//Lombok 
@Getter @Setter @ToString //Genera automaticamente tutti i gettere, i setter e i ToString necessari
public class Attore {
    @Id //Attribuisce la chiave primaria all'attributo sotto di se
    @GeneratedValue(strategy =  GenerationType.IDENTITY) //auto_increment
    @Column(name="cod_attore") //Specifica il nome che avrà il campo in sql
    Long codAttore;
    String nome;
    @Column(name="cod_nascita")
    Long annoNascita;
    //String country;
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name ="country")
    @JsonIdentityReference
    private Country country;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name ="nota_id",referencedColumnName = "id")
    Nota nota;

   

}


