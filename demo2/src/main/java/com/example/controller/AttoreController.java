package com.example.controller;

import com.example.component.MiaClasse;
import com.example.domain.Attore;
import com.example.dto.AttoriPar;
import com.example.service.AttoreService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.*;

@RestController
@RequestMapping("/api/attori")//percorso di base per i root e gli entrry point
@CrossOrigin(origins = "*", maxAge = 3600 )//probelma del corssOrigin; controlla da chi arrivano le richieste sul browser
public class AttoreController {

    //Istanziamento dell'oggetto dipendente dal framewor
    //Istanziamento dell'oggetto dipendente dal framewor
    //@Autowired// di= dependency incection
    AttoreService attoreService;
    MiaClasse miaClasse;
//metodo equivalente della depency injection dell @Autowired
    public AttoreController(AttoreService attoreService/*,MiaClasse miaClasse*/)
        {
            this.attoreService=attoreService;
            //this.miaClasse=miaClasse;
            this.miaClasse= new MiaClasse(); //questa istruzione n caso non volessi utilizzare la di,quindi togliondo il @component, devo instanziarlo con la new per conto mio
        }
    
    @GetMapping("/component/c")
        public ResponseEntity<?> c()
        {
            return new ResponseEntity<String>(miaClasse.getTitolo(),HttpStatus.OK);
        }

    @GetMapping()
    public ResponseEntity<?> findAll(
    @RequestParam(required=false, name="page", defaultValue="0") int page,
    @RequestParam(required=false, name="size", defaultValue="5") int size,
    @RequestParam(required=false, name="ascdesc", defaultValue="asc") String ascdesc,
    @RequestParam(required=false, name="field", defaultValue="codAttore") String field){
        //int page=0:
        //int size=10;
        //String ascdesc="asc";
        //String field="codAttore";
        Sort sort;
        if(ascdesc.equals("asc"))
        {
            sort = Sort.by(field);
        }  
        else{
            sort=Sort.by(field).descending();
        }  
        
        PageRequest request = PageRequest.of(0, 10, sort);
        return new ResponseEntity<List<Attore>>(attoreService.findAllPagge(request),HttpStatus.OK);
    }

    @PostMapping("/dto/{id}")
    public void dto(@RequestBody AttoriPar par, @PathVariable("id") Long id)
        {
            System.out.println(par);
        }
//api PER RITORNARE UN sINGOLO ELEMENTO
    @GetMapping("/{codAttore}")
  
        public ResponseEntity<?> findById(@PathVariable("codAttore") Long codAttore)
            {
                //return new ResponseEntity<Attore>(attoreService.findById(codAttore),HttpStatus.OK);
                Optional<Attore> optAttore= attoreService.findById(codAttore);
                if(optAttore.isPresent())// controlla se l'alttore è stato trovato
                    {
                        Attore attore = optAttore.get();// recupero l'ogetto di tipo attore
                        return new ResponseEntity<Attore>(attore,HttpStatus.OK);
                    }
                return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
            }
//POST PER AGGIUNGER UN ELEMENTO    
    @PostMapping()
    public ResponseEntity<?> save(@RequestBody Attore attore)
        {
            return new ResponseEntity<Void>(HttpStatus.OK);
        }

    @DeleteMapping("/{codAttore}")
    public ResponseEntity<?> deleteById(@PathVariable("codAttore") Long codAttore){
        Optional<Attore>optAttore=attoreService.deleteById(codAttore);
        if(optAttore.isPresent())
            {
                return new ResponseEntity<Void>(HttpStatus.OK);
                
            }
        return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    }

    @PutMapping()
    public ResponseEntity<?> put(@RequestBody Attore attore)
    {
        Optional<Attore> optAttore=attoreService.put(attore);
        if(optAttore.isPresent())
            {
                return new ResponseEntity<Void>(HttpStatus.OK);
            }
        return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    }
    


}


