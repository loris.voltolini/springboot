package com.example;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;


// http://localhost:8080/swagger-ui/

@Configuration //Indica al framework in faser di bootstrap di eseguire il metodo marcato con Bean
public class SwaggerConfig {//Swagger server per autodocumentare le api del nostro progetto

    @Bean // esegui questo metodo
    public Docket api() { //method chain
        return new Docket(DocumentationType.OAS_30)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
                //.forCodeGeneration(true);
    }
}
