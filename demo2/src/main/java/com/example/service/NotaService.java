package com.example.service;

import java.util.Optional;

import com.example.domain.Attore;
import com.example.domain.Nota;
import com.example.repository.NotaRepository;

import org.springframework.beans.factory.annotation.Autowired;

public class NotaService {
    
    @Autowired
    NotaRepository notaRepository;
    @Autowired
    AttoreService atttoreService;

    public void save(Nota nota, Long codAttore){
        notaRepository.save(nota);
        Optional<Attore> optAttore=atttoreService.findById(codAttore);
        if(optAttore.isPresent()){
            Attore attore=optAttore.get();
            attore.setNota(nota);
            atttoreService.save(attore);
        }
    }
    
}
